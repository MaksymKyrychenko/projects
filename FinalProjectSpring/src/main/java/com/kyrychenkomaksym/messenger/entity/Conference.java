package com.kyrychenkomaksym.messenger.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(name="conference", schema = "public")
public class Conference {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "start_at", nullable = false)
    private LocalDateTime starts_at;
    @Column(name = "duration")
    private Integer currentDuration;
    @Column(name = "max_duration", nullable = false)
    private Integer maxDuration;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User creator;
    @ManyToMany
    @JoinColumn(name = "registered_user_id")
    private List<User> registeredUsers;
    @OneToMany
    @JoinColumn(name = "report_id")
    private List<Report> reports;
    @JoinColumn(name = "speaker_id")
    @ManyToMany
    private List<User> speakers;

    public Conference(String title, LocalDateTime starts_at, Integer maxDuration, User creator) {
        this.title = title;
        this.starts_at = starts_at;
        this.creator = creator;
        this.maxDuration = maxDuration;
    }

    public Integer getUsersQuantity() {
        return registeredUsers.size();
    }

    public Integer getReportsQuantity() {
        return reports.size();
    }

    public Integer getCurrentDuration() {
        return reports.stream().mapToInt(Report::getReportDuration).sum();
    }
}