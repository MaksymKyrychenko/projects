package com.kyrychenkomaksym.messenger.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(name="report", schema = "public")
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "maxDuration", nullable = false)
    private Integer reportDuration;
    @JoinColumn(name = "speaker_id")
    @OneToOne
    private User speaker;
    @JoinColumn(name = "request_speaker_id")
    @ManyToMany
    private List<User> requests;

    public Report(String title, Integer reportDuration) {
        this.title = title;
        this.reportDuration = reportDuration;
    }
}