package com.kyrychenkomaksym.messenger.entity;
import org.springframework.security.core.GrantedAuthority;

import com.kyrychenkomaksym.messenger.controller.RegexContainer;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(name="user",
        uniqueConstraints={@UniqueConstraint(columnNames={"email"})}, schema = "public")
public class User implements UserDetails {
    @Id
    @GeneratedValue (strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;
    @Pattern(regexp = RegexContainer.REGEX_NAME)
    @Column(name = "name", nullable = false)
    private String name;
    @Pattern(regexp = RegexContainer.REGEX_NAME)
    @Column(name = "surname", nullable = false)
    private String surname;
    @Pattern(regexp = RegexContainer.REGEX_EMAIL)
    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private RoleType role;
    @Size(min = 8)
    @Column(name = "password", nullable = false)
    private String password;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<RoleType> authorities = new ArrayList<RoleType>();
        authorities.add(getRole());
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getUsername() {
        return email;
    }

}