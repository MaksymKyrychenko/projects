package com.kyrychenkomaksym.messenger.controller;

import com.kyrychenkomaksym.messenger.entity.RoleType;
import com.kyrychenkomaksym.messenger.entity.User;
import com.kyrychenkomaksym.messenger.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;

@Controller
public class RegistrationController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @GetMapping(value = "/registration")
    public String registration(User user) {
        return "registration";
    }


    @PostMapping("/registration")
    public String addUser(@Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "registration";
        }
        if (userRepository.findByEmail(user.getEmail()).isPresent()){
            model.addAttribute("uniqueLoginError", "This email is already registered!");
            return "registration";
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return "/login";
    }

}