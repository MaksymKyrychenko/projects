package com.kyrychenkomaksym.messenger.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class WelcomeController {
    @GetMapping("/")
    public String loginPageRedirect(HttpServletRequest request, HttpServletResponse response, Authentication authResult)
            throws IOException, ServletException {
        if(authResult == null){
            return "welcome";
        }
        String role = authResult.getAuthorities().toArray()[0].toString();
        System.out.println(role);
        switch (role) {
            case "ADMIN":
                response.sendRedirect("/admin");
                break;
            case "SPEAKER":
                response.sendRedirect("/speaker");
                break;
            case "USER":
                response.sendRedirect("/user");
                break;
        }
        return "welcome";
    }
}
