package com.kyrychenkomaksym.messenger.controller;

import com.kyrychenkomaksym.messenger.entity.Conference;
import com.kyrychenkomaksym.messenger.entity.User;
import com.kyrychenkomaksym.messenger.service.ConferenceService;
import com.kyrychenkomaksym.messenger.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@PreAuthorize("hasAuthority('USER')")
@Controller
public class UserController {
    @Autowired
    ConferenceService conferenceService;

    @GetMapping("/user")
    public String user() {
        return "user";
    }

    @GetMapping("/userConferences")
    public String showConferences(Model model, @AuthenticationPrincipal User user) {
        model.addAttribute("ConfContainers", conferenceService.getUserConferencesTableObject(user));
        return "userConferences";
    }

    @GetMapping("/detailedUserConference")
    public String showReports(Model model, @RequestParam(name = "conference")Long id) {
        Conference conference = conferenceService.findConferenceById(id);
        model.addAttribute("conference", conference);
        return "detailedUserConference";
    }

    @GetMapping("/registerInConference")
    public String getRegisterInConference(Model model, @RequestParam(name = "conference") Long id) {
        model.addAttribute("conference", id);
        return "registerInConference";
    }

    @PostMapping("/registerInConference")
    public String postRegisterInConference(Model model,
                                           @AuthenticationPrincipal User user, @RequestParam("conference") Long id) {
        if(!conferenceService.registerInConference(user, conferenceService.findConferenceById(id))){
            model.addAttribute("errorRepeatEmailRegistration", "You cant register in one course twice");
        }
        return "redirect:/userConferences";
    }

    @GetMapping("/userRegisteredConferences")
    public String userRegisteredConferences(Model model, @AuthenticationPrincipal User user) {
        return "userRegisteredConferences";
    }

}
