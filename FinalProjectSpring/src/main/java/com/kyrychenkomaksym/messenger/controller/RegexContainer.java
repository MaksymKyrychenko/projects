package com.kyrychenkomaksym.messenger.controller;

public interface RegexContainer {
    String REGEX_NAME = "(^[А-ЩЬЮЯҐІЇЄ][а-щьюяґіїє']{2,32}$)|(^[A-Z][a-z]{2,32}$)";
    String REGEX_EMAIL = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$";
}