package com.kyrychenkomaksym.messenger.controller;

import com.kyrychenkomaksym.messenger.entity.Conference;
import com.kyrychenkomaksym.messenger.entity.Report;
import com.kyrychenkomaksym.messenger.entity.User;
import com.kyrychenkomaksym.messenger.repository.ConferenceRepository;
import com.kyrychenkomaksym.messenger.repository.ReportRepository;
import com.kyrychenkomaksym.messenger.service.AdminService;
import com.kyrychenkomaksym.messenger.service.ConferenceService;
import com.kyrychenkomaksym.messenger.service.ReportService;
import com.kyrychenkomaksym.messenger.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebParam;
import javax.management.BadAttributeValueExpException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
//@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private UserService userService;
    @Autowired
    ConferenceService conferenceService;
    @Autowired
    AdminService adminService;
    @Autowired
    ReportService reportService;

    @GetMapping("/admin")
    public String showMain() {
        return "admin";
    }

    @GetMapping("/newConference")
    public String showCreateConference() {
        return "newConference";
    }

    @PostMapping("/newConference")
    public String createConference(
            @RequestParam(name = "title") String title,
            @RequestParam(name = "start_at") String start,
            @RequestParam(name = "max_duration") Integer maxDuration,
            @AuthenticationPrincipal User user
            ) {
        Conference conference = new Conference(title, LocalDateTime.parse(start,
                DateTimeFormatter.ISO_LOCAL_DATE_TIME), maxDuration, user);
        conferenceService.save(conference);
        return "redirect:/adminConferences";
    }

    @GetMapping("/newReport")
    public String showNewReport(Model model, @RequestParam(name = "conference_id") Long id) {
        model.addAttribute("conference_id", id);
        return "newReport";
    }


    @PostMapping("/newReport")
    public String createReport(
            Model model,
            @RequestParam(name = "title") String title,
            @RequestParam(name = "duration") Integer duration,
            @RequestParam(name = "conference_id") Long id
    ) throws BadAttributeValueExpException {
        Conference conference = conferenceService.findConferenceById(id);
        if(conference.getCurrentDuration() + duration > conference.getMaxDuration()) {
            throw new BadAttributeValueExpException("Maximum conference duration exceeded");
        }
        conference.setCurrentDuration(conference.getCurrentDuration() + duration);
        Report report = new Report(title, duration);
        conference.getReports().add(report);
        reportService.save(report);
        return "redirect:/detailedConference?conference=" + id;
    }

    @GetMapping("/adminConferences")
    public String showConferences(Model model, @AuthenticationPrincipal User admin) {
        model.addAttribute("futureConfContainers", conferenceService.getUserFutureConferencesTableObject(admin));
        model.addAttribute("pastConfContainers", conferenceService.getUserPastConferencesTableObject(admin));
        return "adminConferences";
    }

    @PostMapping("/conferences")
    public String showSortedConferences(
            Model model,
            @RequestParam String sortBy,
            @AuthenticationPrincipal User admin
    ) {
        switch (sortBy) {
            case "Date":
                model.addAttribute("sortedConfList", conferenceService.sortUserConferencesByDateTime(admin));
                break;
            case "Reports quantity":
                model.addAttribute("sortedConfList", conferenceService.sortUserConferencesByReportsQuantity(admin));
                break;
            case "People quantity":
                model.addAttribute("sortedConfList", conferenceService.sortUserConferencesByUsersQuantity(admin));
                break;
        }
       return "conferences";
    }

    @GetMapping("/detailedConference")
    public String showReports(Model model, @RequestParam (name = "conference")Long id) {
        Conference conference = conferenceService.findConferenceById(id);
        model.addAttribute("conference", conference);
        return "detailedConference";
    }

}
