package com.kyrychenkomaksym.messenger.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.kyrychenkomaksym.messenger.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
   // User findByUsername(String userName);
    Optional<User> findByEmail(String email);
}