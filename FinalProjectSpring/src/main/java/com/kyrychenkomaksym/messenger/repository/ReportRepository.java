package com.kyrychenkomaksym.messenger.repository;

import com.kyrychenkomaksym.messenger.entity.Report;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends CrudRepository<Report, Long> {
}