package com.kyrychenkomaksym.messenger.repository;

import com.kyrychenkomaksym.messenger.entity.Conference;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConferenceRepository extends CrudRepository<Conference, Long> { }