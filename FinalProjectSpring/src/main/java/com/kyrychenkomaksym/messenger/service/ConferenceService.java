package com.kyrychenkomaksym.messenger.service;

import com.kyrychenkomaksym.messenger.entity.Conference;
import com.kyrychenkomaksym.messenger.entity.Report;
import com.kyrychenkomaksym.messenger.entity.User;
import com.kyrychenkomaksym.messenger.repository.ConferenceRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class ConferenceService {
    @Autowired
    private ConferenceRepository conferenceRepository;

    @Getter
    @AllArgsConstructor
    public class MyContainer {
        Conference conference;
        Integer reportsQuantity;
        Integer usersQuantity;
    }

    public Conference save(Conference conference) {
        return conferenceRepository.save(conference);
    }

    public List<Conference> findAllConferences() {
        return StreamSupport.stream(conferenceRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<Conference> findUserPastConference(User user) {
        return findAllUserConferences(user).stream()
                .filter(conference -> conference.getStarts_at().plusMinutes(conference.getMaxDuration()).isBefore(LocalDateTime.now()))
                .collect(Collectors.toList());
    }

    public List<Conference> findUserFutureConference(User user) {
        return findAllUserConferences(user).stream()
                .filter(conference -> conference.getStarts_at().isAfter(LocalDateTime.now()))
                .collect(Collectors.toList());
    }

    public List<Conference> sortUserConferencesByDateTime(User user) {
        return findAllUserConferences(user).stream()
                .sorted(Comparator.comparing(Conference::getStarts_at))
                .collect(Collectors.toList());
    }

    public List<Conference> sortUserConferencesByReportsQuantity(User user) {
        return findAllUserConferences(user).stream()
                .sorted(Comparator.comparing(Conference::getReportsQuantity))
                .collect(Collectors.toList());
    }

    public List<Conference> sortUserConferencesByUsersQuantity(User user) {
        return findAllUserConferences(user).stream()
                .sorted(Comparator.comparing(Conference::getUsersQuantity))
                .collect(Collectors.toList());
    }

    public boolean hasConferenceStarted(Conference conference) {
        return conference.getStarts_at().isBefore(LocalDateTime.now());
    }

    public boolean hasConferenceEnded(Conference conference) {
        return conference.getStarts_at().isBefore(LocalDateTime.now().plusMinutes(conference.getCurrentDuration()));
    }

    public List<Conference> findAllUserConferences(User user) {
        if (user.getRole().toString().equals("USER")) {
            return findAllConferences().stream()
                    .collect(Collectors.toList());
        }
        Stream<Conference> confStream = findAllConferences().stream();
        if (user.getRole().toString().equals("ADMIN")) {
            return confStream.filter(conference -> conference.getCreator().getId().equals(user.getId()))
                    .collect(Collectors.toList());
        }
        return confStream.filter(conference -> conference.getSpeakers().stream().
                        anyMatch(user1 -> user1.getId().equals(user.getId()))).
                collect(Collectors.toList());
    }


    public List<Integer> getUserFutureConferencesUsersQuantity(User user) {
        return findUserFutureConference(user).stream().map(Conference::getUsersQuantity)
                .collect(Collectors.toList());
    }

    public List<Integer> getUserPastConferencesUsersQuantity(User user) {
        return findUserPastConference(user).stream().map(Conference::getUsersQuantity)
                .collect(Collectors.toList());
    }

    public List<Integer> getUserFutureConferenceReportsQuantity(User user) {
        return findUserFutureConference(user).stream().map(Conference::getReportsQuantity)
                .collect(Collectors.toList());
    }

    public List<Integer> getUserPastConferenceReportsQuantity(User user) {
        return findUserPastConference(user).stream().map(Conference::getReportsQuantity)
                .collect(Collectors.toList());
    }

    public List<MyContainer> getUserFutureConferencesTableObject(User user) {
        List<Conference> conferences = findUserFutureConference(user);
        List<Integer> conferenceUserQuantity = getUserFutureConferencesUsersQuantity(user);
        List<Integer> conferenceReportsQuantity = getUserFutureConferenceReportsQuantity(user);
        List<MyContainer> myContainers = new ArrayList<>();
        for(int i = 0; i < conferences.size(); i++) {
            myContainers.add(i, new MyContainer(conferences.get(i), conferenceReportsQuantity.get(i),
                    conferenceUserQuantity.get(i)));
        }
        return myContainers;
    }

    public List<MyContainer> getUserPastConferencesTableObject(User user) {
        List<Conference> conferences = findUserPastConference(user);
        List<Integer> conferenceUserQuantity = getUserPastConferencesUsersQuantity(user);
        List<Integer> conferenceReportsQuantity = getUserPastConferenceReportsQuantity(user);
        List<MyContainer> myContainers = new ArrayList<>();
        for(int i = 0; i < conferences.size(); i++) {
            myContainers.add(i, new MyContainer(conferences.get(i), conferenceReportsQuantity.get(i),
                    conferenceUserQuantity.get(i)));
        }
        return myContainers;
    }

    public List<Conference> findAllUserRegisteredConferences(User user) {
        return findAllConferences().stream()
                .filter(conference -> hasConferenceEnded(conference)
                        && conference.getRegisteredUsers().contains(user)).collect(Collectors.toList());
    }

    public List<Conference> findAllUserUserConferences(User user) {
        return findAllConferences().stream()
                .filter(conference -> (!hasConferenceEnded(conference))
                        && (!conference.getRegisteredUsers().contains(user))).collect(Collectors.toList());
    }

    public List<MyContainer> getUserConferencesTableObject(User user) {
        List<Conference> conferences = findAllUserUserConferences(user);
        List<Integer> conferenceReportsQuantity = conferences.stream().map(Conference::getReportsQuantity)
                .collect(Collectors.toList());
        List<Integer> conferenceUsersQuantity = conferences.stream().map(Conference::getUsersQuantity)
                .collect(Collectors.toList());
        List<MyContainer> myContainers = new ArrayList<>();
        for(int i = 0; i < conferences.size(); i++) {
            myContainers.add(i, new MyContainer(conferences.get(i), conferenceReportsQuantity.get(i),
                    conferenceUsersQuantity.get(i)));
        }
        return myContainers;
    }



    public boolean isUserRegistered(User user, Conference conference) {
        return conference.getRegisteredUsers().contains(user);
    }

    public boolean registerInConference(User user, Conference conference) {
        if (isUserRegistered(user, conference)) {
            return false;
        }
        conference.getRegisteredUsers().add(user);
        save(conference);
        return true;
    }

    public Conference findConferenceById(Long id) throws NullPointerException{
        return conferenceRepository.findById(id).orElseThrow(() ->new NullPointerException("No user with that id"));
    }
}