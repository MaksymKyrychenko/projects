package com.kyrychenkomaksym.messenger.service;

import com.kyrychenkomaksym.messenger.entity.Conference;
import com.kyrychenkomaksym.messenger.entity.User;
import com.kyrychenkomaksym.messenger.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByEmail(s).get();
    }

    public List<User> findAllSpeakers(){
        List<User> speakers = userRepository.findAll()
                .stream().filter(element -> element.getRole().toString().equals("SPEAKER"))
                .collect(Collectors.toList());
        return speakers;
    }

    public Optional<User> findSpeakerById(Long speaker_id) {
        return findAllSpeakers().stream()
                .filter(speaker -> speaker.getId().equals(speaker_id))
                .findAny();
    }
}
