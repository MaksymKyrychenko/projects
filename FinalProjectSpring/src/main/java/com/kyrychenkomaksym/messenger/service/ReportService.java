package com.kyrychenkomaksym.messenger.service;

import com.kyrychenkomaksym.messenger.entity.Conference;
import com.kyrychenkomaksym.messenger.entity.Report;
import com.kyrychenkomaksym.messenger.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService {
    @Autowired
    ReportRepository reportRepository;

    public List<Report> findAllReportsInConference(Conference conference) {
        return conference.getReports();
    }

    public Report save(Report report) {
        return reportRepository.save(report);
    }
}