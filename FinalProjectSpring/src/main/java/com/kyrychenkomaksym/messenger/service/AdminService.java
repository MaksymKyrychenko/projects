package com.kyrychenkomaksym.messenger.service;

import com.kyrychenkomaksym.messenger.entity.Conference;
import com.kyrychenkomaksym.messenger.entity.Report;
import com.kyrychenkomaksym.messenger.entity.User;
import com.kyrychenkomaksym.messenger.repository.ConferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.core.support.DefaultCrudMethods;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class AdminService {
    @Autowired
    ConferenceService conferenceService;

    /*public void bindReportToSpeaker(Conference conference, User speaker, Report report) {
        report.setSpeaker(speaker);
        conference.getFreeSpeakers().remove(speaker);
        conference.getSpeakers().add(speaker);
    }*/

    public void changeSpeakerReport(User speaker, Report newReport, Report oldReport) {
        newReport.setSpeaker(speaker);
        oldReport.setSpeaker(null);
    }
}