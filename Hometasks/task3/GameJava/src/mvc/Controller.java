package mvc;

import java.util.Scanner;

/**
 * Created by Kyrychenko Maksym on 01.07.2021.
 */
public class Controller {
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void startGame() {
        model.setPrimaryBounds(GlobalConstants.PRIMARY_LOWER_BOUND, GlobalConstants.PRIMARY_UPPER_BOUND);
        model.setUnknownNumber();
        Scanner sc = new Scanner(System.in);
        while (true) {
            if(model.getUserInputs().size() != 0) {
                view.printMessage("Previous inputs: " + model.getUserInputs().toString());
            }
            view.printMessage(View.HINT + model.getLowerBound() + " and " + model.getUpperBound());
            String inputStr = sc.nextLine();
            Integer inputNumber;
            //Data type check
            try {
                inputNumber = Integer.valueOf(inputStr);
            } catch (NumberFormatException ex) {
                view.printMessage(View.INCORRECT_DATA_TYPE);
                continue;
            }
            if (inputStr.contains(".") || inputStr.contains(",")) {
                view.printMessage(View.INCORRECT_DATA_TYPE);
                continue;
            }
            //Check if value is inside the bounds
            if(inputNumber < model.getLowerBound()) {
                view.printMessage(inputNumber + View.INPUT_UNDER_BOUND);
                continue;
            }
            if(inputNumber > model.getUpperBound()) {
                view.printMessage(inputNumber + View.INPUT_OVER_BOUND);
                continue;
            }
            model.addInputInArray(inputNumber);
            int i = model.processInput(inputNumber);
            if (i == -1) {
                view.printMessage(inputNumber + View.INPUT_IS_LESS);
                continue;
            }
            if (i == 0) {
                view.printMessage(inputNumber + View.SUCCESSFUL_ATTEMPT + model.getUserInputs().toString());
                break;
            }
            if (i == 1) {
                view.printMessage(inputNumber + View.INPUT_IS_BIGGER);
                continue;
            }
        }
    }
}