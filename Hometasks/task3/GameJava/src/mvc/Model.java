package mvc;

import java.util.Random;
import java.util.ArrayList;

/**
 * Created by Kyrychenko Maksym on 01.07.2021.
 */
public class Model {
    private int unknownNumber;

    private int lowerBound;
    private int upperBound;

    private ArrayList<Integer> userInputs = new ArrayList<Integer>();

    public void addInputInArray(int input) {
        this.userInputs.add(input);
    }

    public void setUnknownNumber() {
        this.unknownNumber = rand();
    }

    public void setPrimaryBounds(int lowerBound, int upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    public int processInput(int input){
        if(input < this.unknownNumber) {
            this.lowerBound = input;
            return -1;
        }
        if(input == this.unknownNumber) {
            return 0;
        }
        this.upperBound = input;
        return 1;
    }

    //Makes random number [1 - 99]
    public int rand() {
        int num = (int)Math.ceil(Math.random()*
                (upperBound - lowerBound - 1) + lowerBound);
        return num;
    }

    public int getUnknownNumber() {
        return unknownNumber;
    }
    public int getLowerBound() {
        return lowerBound;
    }
    public int getUpperBound() {
        return upperBound;
    }
    public ArrayList<Integer> getUserInputs() {
        return userInputs;
    }

}