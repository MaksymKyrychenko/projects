package mvc;

/**
 * Created by Kyrychenko Maksym on 01.07.2021.
 */
public interface GlobalConstants {
    // Constants
    int PRIMARY_LOWER_BOUND = 0;
    int PRIMARY_UPPER_BOUND = 100;
}