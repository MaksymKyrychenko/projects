package Tests;

import mvc.GlobalConstants;
import org.junit.*;
import mvc.Model;

/**
 * Created by Kyrychenko Maksym on 01.07.2021.
 */
public class TestModel{
    private static Model model;
    @BeforeClass
    public static void initialize(){
        model = new Model();
        model.setUnknownNumber();
        model.setPrimaryBounds(GlobalConstants.PRIMARY_LOWER_BOUND, GlobalConstants.PRIMARY_UPPER_BOUND);
    }

    @Test
    public void testRandOnGap(){
        for(int i = 0; i < 100000; i++) {
            int res = model.rand();
            if(res > 99 || res < 1) Assert.fail("Nubmer is out of bounds");
        }
    }

    @Test
    public void testProcessInputOnLowerValues() {
        if(model.processInput(model.getUnknownNumber() - 1) != -1) Assert.fail();
    }

    @Test
    public void testProcessInputOnHigherValues() {
        if(model.processInput(model.getUnknownNumber() + 1) != 1) Assert.fail();
    }

    @Test
    public void testProcessInputOnCorrectValues() {
        if(model.processInput(model.getUnknownNumber()) != 0) Assert.fail();
    }
}