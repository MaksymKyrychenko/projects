import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Kyrychenko Maksym on 23.07.2021
 */
public class MyStream {
    public static void main(String[] args) {
        int[] myInts = {2, 3, 1, 102, 12, 13, 27, 33, 44, 0, -13, -12, -55, 11, 83, 19, -99};
        System.out.print("Array at start: ");
        IntStream.of(myInts).forEach(i -> System.out.print(i + " "));
        Double avg = IntStream.of(myInts).average().getAsDouble();
        System.out.println("\nAverage: " + avg);
        Integer minValue = IntStream.of(myInts).min().getAsInt();
        Integer minValueIndex = IntStream.range(0, myInts.length).filter(i -> minValue == myInts[i]).findFirst().getAsInt();
        System.out.println("Min:  " + minValue + " index: " + minValueIndex);
        long equalsZero = IntStream.of(myInts).filter(x -> x == 0).count();
        System.out.println("Equals zero " + equalsZero + " elements");
        long greaterThanZero = IntStream.of(myInts).filter(x -> x > 0).count();
        System.out.println("Greater than  zero " + greaterThanZero + " elements");
        final int MULTIPLE_BY = 4;
        int[] valuesAfterMultiplication = IntStream.of(myInts).map(x -> x * MULTIPLE_BY).toArray();
        System.out.print("Array after multiplication: ");
        IntStream.of(valuesAfterMultiplication).forEach(i -> System.out.print(i + " "));
    }
}