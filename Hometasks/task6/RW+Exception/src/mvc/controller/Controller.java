package mvc.controller;

import mvc.MyExceptions.NotUniqueLoginException;
import mvc.model.User;
import mvc.view.View;
import mvc.model.UsersBook;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.*;

import netscape.javascript.JSException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import static mvc.view.TextConstants.*;
import static mvc.controller.RegexContainer.*;


/**
 * That class contains as fields classes {@link mvc.model.User}, {@link mvc.view.View}
 * It takes user input, validates it, notifies user through class {@link mvc.view.View}
 * about errors or successful input and sends input as data to class {@link mvc.model.User}
 * @author Kyrychenko Maksym
 * @version 2.0
 */
public class Controller {
    /** Field {@link mvc.model.User} */
    private User user;
    /** Field {@link mvc.view.View} */
    private View view;
    /** {@link Scanner}*/
    private Scanner sc;


    /** class's constructor
     * @param user {@link mvc.model.User}
     * @param view {@link mvc.view.View}
     */
    public Controller(User user, View view) {
        this.user = user;
        this.view = view;
        this.sc = new Scanner(System.in);
    }

    /**
     * General logic of processing user's input
     */
    public void processUser() {
        while (true) {
            view.printMessage(CHOOSE_LANGUAGE);
            chooseLocaleWithScanner(sc);
            getUserData(sc);
            try {
                UsersBook.addUserToList(user);
            } catch (NotUniqueLoginException ex) {
                System.out.println(ex.getMessage());
                UsersBook.clearUsersList();
                continue;
            }
            UsersBook.writeDataInFile();
            view.printMessage(SUCCESSFUL_ATTEMPT);
            break;
        }
    }

    /**
     * That method asks user to select locale,
     * validates input and changes locale
     * @param sc - {@link Scanner}
     */
    public void chooseLocaleWithScanner(Scanner sc){
        Integer i;
        while(true){
            try{
                 i = Integer.parseInt(sc.next());
            }
            catch (NumberFormatException ex){
                view.printMessage(INCORRECT_CHOOSE_LANGUAGE);
                continue;
            }
            if(i != 1 && i != 2){
                view.printMessage(INCORRECT_CHOOSE_LANGUAGE);
            }
            if(i == 1){
                view.chooseLocale("en");
                return;
            }
            if(i == 2){
                view.chooseLocale("ua");
                return;
            }
        }
    }

    /**
     * That method gets user input (user's name and nickname),
     * validates it and sends as data to model {@link mvc.model.User}
     * @param sc - class {@link Scanner}
     */
    public void getUserData(Scanner sc) {
        UtilityController utilityController =
                new UtilityController(sc, view);
        String str = (String.valueOf(View.bundle.getLocale()).equals("ua"))
                ? REGEX_NAME_UKR : REGEX_NAME_LAT;
        user.setName(utilityController.inputNameWithScanner
                (INPUT_FIRST_NAME, INCORRECT_NAME, str));
        user.setNickname(utilityController.inputNameWithScanner
                (INPUT_NICKNAME, INCORRECT_NICKNAME, REGEX_LOGIN));
    }
}