package mvc.model;

import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

/**
 * That class's fields contain all information about user. Its methods process data and add new user to txt file.
 * @author Maksym Kyrychenko
 * @version 2.0
 */
public class User{
    /** Field user's name */
    private String name;
    /** Field user's nickname */
    private String nickname;

    public String getName(){
        return this.name;
    }

    public String getNickname(){
        return this.nickname;
    }

    /**
     * Initialize field {@link User#name}
     * @see User
     * @param name - user's name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * initialize field {@link User#nickname}
     * @see User
     * @param nickname - user's nickname
     */
    public void setNickname(String nickname){
        this.nickname = nickname;
    }

}