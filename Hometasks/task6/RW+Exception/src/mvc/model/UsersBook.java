package mvc.model;

import mvc.MyExceptions.NotUniqueLoginException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * That class contains list of existing users.
 * @author Maksym Kyrychenko
 * @version 1.0
 */
public class UsersBook {
    private static ArrayList<User> users = new ArrayList<User>();

    /**
     * That method writes updated data in JSON format to txt file.
     */
    public static void writeDataInFile(){
        JSONArray usersJson= new JSONArray();
        for (User user : users){
            JSONObject userJson = new JSONObject();
            userJson.put("name", user.getName());
            userJson.put("nickname", user.getNickname());
            usersJson.add(userJson);
        }
        System.out.println(usersJson.toString());
        try(FileWriter writer = new FileWriter("Users.txt", false)) {
            writer.write(usersJson.toJSONString());
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * That method adds new user's data to users list.
     * @param user {@link User}
     * @throws NotUniqueLoginException
     */
    public static void addUserToList(User user)throws NotUniqueLoginException {
        if(checkSameNickname(user.getNickname())) {
            throw new NotUniqueLoginException("User with that nickname already exists");
        }
        users.add(user);
    }

    /**
     * That method checks if there is other user with same nickname.
     * @param nickname {@link User#getNickname()}
     * @return
     */
    public static boolean checkSameNickname(String nickname){
        getAllUsers();
        for(User user : users){
            if(user.getNickname().equals(nickname)){
                return true;
            }
        }
        return false;
    }

    /**
     * That method fills users list with data from txt file.
     */
    public static void getAllUsers(){
        JSONParser parser = new JSONParser();
        String jsonText = "";
        try {
            jsonText = new String(Files.readAllBytes(Paths.get("Users.txt")));
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
            System.exit(1);
        }
        if(jsonText.equals("")) {
            return;
        }
        try {
            JSONArray usersJson = (JSONArray) parser.parse(jsonText);
            for (Object obj : usersJson){
                User usr = new User();
                usr.setName(((JSONObject) obj).get("name").toString());
                usr.setNickname(((JSONObject) obj).get("nickname").toString());
                users.add(usr);
            }

        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
            System.exit(1);
        }
    }

    /**
     * That method clears users list
     */
    public static void clearUsersList(){
        users.clear();
    }
}