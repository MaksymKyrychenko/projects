package mvc;

import mvc.controller.Controller;
import mvc.model.User;
import mvc.model.User;
import mvc.view.View;

/**
 * That class runs the program
 * @author Maksym Kyrychenko
 * @version 1.0
 */
public class Main{
    public static void main(String[] ars) {
        Controller controller = new Controller(new User(), new View());
        controller.processUser();
    }
}