package mvc.MyExceptions;

/**
 * That exception appears when user tries
 * to register with login that already exists
 * @author Maksym Kyrychenko
 * @version 1.0
 */
public class NotUniqueLoginException extends Exception{
    public NotUniqueLoginException(String message){
        super(message);
    }
}