package mvc.view;

import java.util.Locale;
import java.util.ResourceBundle;
import static mvc.view.TextConstants.*;

/**
 * That class contains information about locale,
 * method that switches locales
 * and utility method that prints message
 * @author Maksym Kyrychenko
 * @version 2.0
 */
public class View {
    static String MESSAGES_BUNDLE_NAME = "messages";
    public static ResourceBundle bundle = ResourceBundle.getBundle(
            MESSAGES_BUNDLE_NAME,
            new Locale("en"));

    /**
     * Utility method. Prints message in user's terminal
     * @param message
     */
    public void printMessage(String message) {
        System.out.println(bundle.getString(message));
    }

    /**
     * That method switches locales
     * @param iso - localization in ISO format
     */
    public void chooseLocale(String iso) {
        if(iso.equals("en")){
            bundle = ResourceBundle.getBundle(
                    MESSAGES_BUNDLE_NAME,
                    new Locale("en")
            );
        }
        if(iso.equals("ua")){
            bundle = ResourceBundle.getBundle(
                    MESSAGES_BUNDLE_NAME,
                    new Locale("ua", "UA"));
        }
    }
}