package mvc;

public class View {
    public static final String INPUT_IS_LESS = " is less than unknown number";
    public static final String INPUT_IS_BIGGER = " is bigger than unknown number";
    public static final String INPUT_UNDER_BOUND = " is less than lower bound! Unknown number" +
            "is between 0 and 100";
    public static final String INPUT_OVER_BOUND = " is bigger than upper bound! Unknown number" +
            "is between 0 and 100";
    public static final String SUCCESSFUL_ATTEMPT = " is right! You won!\n All inputs: ";
    public static final String HINT = "Please enter a number between ";
    public static final String INCORRECT_DATA_TYPE = "You must enter an integer!";

    public void printMessage(String message) {
        System.out.println(message);
    }
}