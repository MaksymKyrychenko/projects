package mvc;

import java.util.Random;
import java.util.ArrayList;

public class Model {
    public ArrayList<Integer> userInputs = new ArrayList<Integer>();
    public final int MIN;
    public final int MAX;
    public final int NUMBER;

    public Model() {
        this.NUMBER = rand();
        this.MIN = 0;
        this.MAX = 100;
    }

    public Model(int min, int max) {
        this.NUMBER = rand(min, max);
        this.MIN = min;
        this.MAX = max;
    }

    public void addInputInArray(int input) {
        this.userInputs.add(input);
    }

    public int processInput(int input){
        if(input < MIN) {
            return -2;
        }
        if(input > MAX) {
            System.out.println();
            return 2;
        }
        if(input < this.NUMBER) {
            System.out.println();
            return -1;
        }
        if(input == this.NUMBER) {
            return 0;
        }
        return 1;
    }

    private int rand() {
        Random random = new Random();
        int num = random.nextInt(101);
        return num;
    }

    private int rand(int min, int max) {
        Random random = new Random();
        int diff = max - min;
        int num = random.nextInt(diff + 1) + min;
        return num;
    }
}