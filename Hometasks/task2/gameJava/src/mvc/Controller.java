package mvc;

import java.util.Scanner;

public class Controller {
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void startGame() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            if(model.userInputs.size() != 0) {
                view.printMessage("Previous inputs: " + model.userInputs.toString());
            }
            view.printMessage(View.HINT + model.MIN + " and " + model.MAX);
            String inputStr = sc.nextLine();
            Integer inputNumber;
            try {
                inputNumber = Integer.valueOf(inputStr);
            } catch (NumberFormatException ex) {
                view.printMessage(View.INCORRECT_DATA_TYPE);
                continue;
            }
            if (inputStr.contains(".") || inputStr.contains(",")) {
                view.printMessage(View.INCORRECT_DATA_TYPE);
                continue;
            }
            model.addInputInArray(inputNumber);
            int i = model.processInput(inputNumber);
            if (i == -2) {
                view.printMessage(inputNumber + View.INPUT_UNDER_BOUND);
                continue;
            }
            if (i == -1) {
                view.printMessage(inputNumber + View.INPUT_IS_LESS);
                continue;
            }
            if (i == 0) {
                view.printMessage(inputNumber + View.SUCCESSFUL_ATTEMPT + model.userInputs.toString());
                break;
            }
            if (i == 1) {
                view.printMessage(inputNumber + View.INPUT_IS_BIGGER);
                continue;
            }
            if (i == 2) {
                view.printMessage(inputNumber + View.INPUT_OVER_BOUND);
                continue;
            }
        }
    }
}