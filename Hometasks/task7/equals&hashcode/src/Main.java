/**
 * Created by Kyrychenko Maksym on 11.07.2021
 */
public class Main {
    public static void main(String[] args){
        Student st1 = new Student("Max", 20, "KP-92");
        Student st2 = new Student("Katya", 19, "KP-92");
        Student st3 = new Student("Ivan", 20, "KP-92");
        Student st4 = new Student("Max", 20, "KP-92");
        System.out.println("st1 hashCode: " + st1.hashCode());
        System.out.println("st2 hashCode: " + st2.hashCode());
        System.out.println("st3 hashCode: " + st3.hashCode());
        System.out.println("st4 hashCode: " + st4.hashCode());
        System.out.println("st1 equals st2: " + st1.equals(st2));
        System.out.println("st1 equals st3: " + st1.equals(st3));
        System.out.println("st1 equals st4: " + st1.equals(st4));
    }
}