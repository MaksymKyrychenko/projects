/**
 * Created by Kyrychenko Maksym on 11.07.2021
 */
public class Student {
    public String name;
    public int age;
    public String group;

    public Student(String name, int age, String group){
        this.name = name;
        this.age = age;
        this.group = group;
    }

    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(getClass() == obj.getClass()){
            Student temp = (Student) obj;
            return this.name.equals(temp.name) && this.age == temp.age
                    && this.group.equals(temp.group);
        }
        else {
            return false;
        }
    }

    public int hashCode(){
        return (int)(41 * age + (name == null ? 0 : name.hashCode()) + (group == null ? 0 : group.hashCode()));
    }
}