
public class CarModel{
    private String modelName;
    private String bodyNumber;
    private int yearOfManufacture;

    public CarModel(){
        this.modelName = "";
        this.bodyNumber = "";
        this.yearOfManufacture = 0;
    }

    public CarModel(String modelName, String bodyNumber, int yearOfManufacture){
        this.modelName = modelName;
        this.bodyNumber = bodyNumber;
        this.yearOfManufacture = yearOfManufacture;
    }

    public String getModelName(){
        return this.modelName;
    }

    public String getBodyNumber(){
        return this.bodyNumber;
    }

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public void setBodyNumber(String bodyNumber) {
        this.bodyNumber = bodyNumber;
    }

    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }
}