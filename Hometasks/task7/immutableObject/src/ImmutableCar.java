/**
 * Created by Kyrychenko Maksym on 11.07.2021
 */
public final class ImmutableCar{
    private final String brand;
    private final CarModel model;

    public ImmutableCar(String brand, CarModel model){
        this.brand = brand;
        CarModel cloneModel = new CarModel(model.getModelName(), model.getBodyNumber(), model.getYearOfManufacture());
        this.model = cloneModel;
    }

    public String getBrand(){
        return brand;
    }

    public CarModel getModel() {
        CarModel cloneModel = new CarModel(this.model.getModelName(),
                this.model.getBodyNumber(), this.model.getYearOfManufacture());
        return cloneModel;
    }

    public ImmutableCar setBrand(String brand){
        return new ImmutableCar(brand, this.model);
    }

    public ImmutableCar setModel(CarModel model){
        return new ImmutableCar(this.brand, model);
    }
}