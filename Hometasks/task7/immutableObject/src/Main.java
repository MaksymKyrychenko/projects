/**
 * Created by Kyrychenko Maksym on 11.07.2021
 */
public class Main {
    public static void main(String[] args){
        ImmutableCar immutableCar = new ImmutableCar("Honda",
                new CarModel("Accord", "8, restyle", 2012));
        System.out.println("Brand: " + immutableCar.getBrand() +
                "\nmodelName: " + immutableCar.getModel().getModelName() +
                "\nbodyNumber: " + immutableCar.getModel().getBodyNumber() +
                "\nyearOfManufacture: " + immutableCar.getModel().getYearOfManufacture());
        immutableCar.setBrand("BMW");
        immutableCar.setModel(new CarModel("M5", "E60", 2006));
        System.out.println("Brand: " + immutableCar.getBrand() +
                "\nmodelName: " + immutableCar.getModel().getModelName() +
                "\nbodyNumber: " + immutableCar.getModel().getBodyNumber() +
                "\nyearOfManufacture: " + immutableCar.getModel().getYearOfManufacture());
        immutableCar.getModel().setBodyNumber("F90");
        immutableCar.getModel().setYearOfManufacture(2020);
        System.out.println("Brand: " + immutableCar.getBrand() +
                "\nmodelName: " + immutableCar.getModel().getModelName() +
                "\nbodyNumber: " + immutableCar.getModel().getBodyNumber() +
                "\nyearOfManufacture: " + immutableCar.getModel().getYearOfManufacture());
    }
}