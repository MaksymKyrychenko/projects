package mvc.view;

/**
 * That interface contains notifications for user
 * as constants linked with fields in property files
 * @author Maksym Kyrychenko
 * @version 1.0
 */
public interface TextConstants {
    String INCORRECT_NAME = "input.incorrect.first.name";
    String INPUT_FIRST_NAME = "input.first.name";
    String INCORRECT_NICKNAME = "input.incorrect.nickname";
    String INPUT_NICKNAME = "input.nickname";
    String SUCCESSFUL_ATTEMPT = "input.successful.attempt";
    String CHOOSE_LANGUAGE = "input.choose.language";
    String INCORRECT_CHOOSE_LANGUAGE = "input.incorrect.choose.language";
}