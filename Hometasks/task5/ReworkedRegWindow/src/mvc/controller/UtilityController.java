package mvc.controller;

import mvc.view.View;
import java.util.Scanner;

/**
 * Class Utility Controller
 * Contains fields {@link Scanner} and {@link View}
 * and method that takes user input and validates it.
 * @author Maksym Kyrychenko
 * @version 1.0
 */
public class UtilityController {
    /** {@link Scanner}*/
    private Scanner sc;
    /** {@link View}*/
    private View view;

    /**Constructor*/
    public UtilityController(Scanner sc, View view) {
        this.sc = sc;
        this.view = view;
    }

    /**
     * Validates user input and sends it to controller.
     * @param message - notification about input
     * @param errorNotification notification about input error
     * @param regex - regular expression
     * @return - user input
     */
    String inputNameWithScanner(String message, String errorNotification, String regex) {
        String res;
        view.printMessage(message);
        while( !(sc.hasNext() && (res = sc.next()).matches(regex))) {
            view.printMessage(errorNotification);
        }
        return res;
    }
}