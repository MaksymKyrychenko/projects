package mvc.controller;

/**
 * That interface contains regular expressions for
 * user's name and nickname
 * in english and ukrainian
 * @author Maksym Kyrychenko
 * @version 1.0
 */
public interface RegexContainer {
    String REGEX_NAME_UKR = "^[А-ЩЬЮЯҐІЇЄ][а-щьюяґіїє']{1,32}$";
    String REGEX_NAME_LAT = "^[A-Z][a-z]{1,32}$";
    String REGEX_LOGIN = "^[A-Za-z0-9_-]{8,32}$";
}