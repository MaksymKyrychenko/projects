import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * Created by Kyrychenko Maksym on 21.07.2021
 */
public class ArrayListWithoutRemove extends ArrayList {
    @Override
    public Object set(int index, Object element) {
        if(super.get(index) != null){
            System.out.println("You cant remove data from that array");
            return null;
        }
        else{
            return super.set(index, element);
        }
    }

    @Override
    public boolean remove(Object o) {
        System.out.println("You cant remove data from that array");
        return false;
    }

    @Override
    public Object remove(int index) {
        System.out.println("You cant remove data from that array");
        return null;
    }

    @Override
    public void clear() {
        System.out.println("You cant remove data from that array");
    }

    @Override
    public boolean removeAll(Collection c) {
        System.out.println("You cant remove data from that array");
        return false;
    }

    @Override
    public boolean removeIf(Predicate filter) {
        System.out.println("You cant remove data from that array");
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        System.out.println("You cant remove data from that array");
        return false;
    }

    @Override
    public void replaceAll(UnaryOperator operator) {
        System.out.println("You cant remove data from that array");
    }

}