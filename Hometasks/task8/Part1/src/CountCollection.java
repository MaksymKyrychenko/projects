import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Kyrychenko Maksym on 21.07.2021
 */
public class CountCollection {
    private ArrayList<Integer> integers = new ArrayList<>();
    private ArrayList<Integer> counterPositiveElements = new ArrayList<>();
    private ArrayList<Integer> counterNegativeElements = new ArrayList<>();
    private int lowerBound = 0;
    private int upperBound = 0;

    public void countElements(){
        for(Integer i : integers){
            if(i >= 0){
                changeCounter(counterPositiveElements, i);
            }
            if(i < 0){
                Integer absI = -i;
                changeCounter(counterNegativeElements, absI);
            }
        }
        printCounts();
    }

    public void changeCounter(ArrayList<Integer> counterList, Integer i){
        if(counterList.get(i) == null){
            counterList.set(i, 1);
        }
        else{
            counterList.set(i, counterList.get(i) + 1);
        }
    }

    public void ensureArrSize(ArrayList<Integer> list, int size) {
        list.ensureCapacity(size);
        while (list.size() < size) {
            list.add(null);
        }
    }

    public void printCounts(){
        System.out.println(integers);
        for(int i = 0; i < counterPositiveElements.size(); i++){
            if(counterPositiveElements.get(i) != null){
                System.out.println(i + " - " + counterPositiveElements.get(i));
            }
        }
        for(int i = 0; i < counterNegativeElements.size(); i++){
            if(counterNegativeElements.get(i) != null){
                System.out.println(-i + " - " + counterNegativeElements.get(i));
            }
        }
    }

    public void fillArrayWithRandom(int quantity, int max, int min){
        ensureArrSize(counterNegativeElements, -min + 1);
        ensureArrSize(counterPositiveElements, max + 1);
        for(int i = 0; i < quantity; i++){
            int randInt = (int) ((Math.random() * (max - min)) + min);
            System.out.println(randInt);
            integers.add(randInt);
        }
    }
}

