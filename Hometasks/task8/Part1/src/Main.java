/**
 * Created by Kyrychenko Maksym on 21.07.2021
 */
import java.lang.Class;
public class Main {
    public static void main(String[] args){
        CountCollection count = new CountCollection();
        count.fillArrayWithRandom(100, 8, -8);
        count.countElements();
    }
}