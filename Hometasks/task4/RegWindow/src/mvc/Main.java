package mvc;

/**
 * That class runs the program
 * @author Maksym Kyrychenko
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);
        controller.processUser();
    }
}