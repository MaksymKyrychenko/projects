package mvc;

import java.util.Scanner;
import java.util.regex.*;

/**
 * That class contains as fields classes {@link mvc.Model}, {@link mvc.View}
 * It takes user input, validates it, notifies user through class {@link mvc.View}
 * about errors or successful input and sends input as data to class {@link mvc.Model}
 * @author Kyrychenko Maksym
 * @version 1.0
 */
public class Controller {
    /** That field contains regular expression for user's name */
    public final String NAME_REGEX = "^[A-Z]{1,2}[a-zA-Z-]{1,32}$";
    /** That field contains regular expression for user's nickname */
    public final String NICKNAME_REGEX = "^[a-zA-Z]{1,}[a-zA-Z0-9-_]{7,}$";

    /** Field {@link mvc.Model} */
    private Model model;
    /** Field {@link mvc.View} */
    private View view;

    /** class's constructor
     * @param model {@link mvc.Model}
     * @param view {@link mvc.View}
     */
    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser() {
        Scanner sc = new Scanner(System.in);
        getUserName(sc);
        getUserNickname(sc);
        model.writeDataInFile();
        view.printMessage(View.SUCCESSFUL_INPUT);
    }

    /**
     * That method gets user input (user's name),
     * validates it and sends as data to model {@link mvc.Model}
     * @param sc - class {@link Scanner}
     */
    public void getUserName(Scanner sc) {
        view.printMessage("Write your name, please");
        while(true) {
            String userInput = sc.next();
            if (!userInput.matches(NAME_REGEX)) {
                view.printMessage(View.INCORRECT_NAME);
                view.printMessage("Try again: ");
                continue;
            }
            model.setName(userInput);
            break;
        }
    }

    /**
     * That method gets user input (user's nickname),
     * validates it and sends as data to model {@link mvc.Model}
     * @param sc - class {@link Scanner}
     */
    public void getUserNickname(Scanner sc) {
        view.printMessage("Write your nickname, please");
        while(true) {
            String userInput = sc.next();
            if (!userInput.matches(NICKNAME_REGEX)) {
                view.printMessage(View.INCORRECT_NICKNAME);
                view.printMessage("Try again ");
                continue;
            }
            model.setNickname(userInput);
            break;
        }
    }
}