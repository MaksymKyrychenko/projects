package mvc;

import java.io.FileWriter;
import java.io.IOException;

/**
 * That class's fields contain all information about user. Its methods process data and add new user to txt file.
 * @author Maksym Kyrychenko
 * @version 1.0
 */
public class Model{
    /** Field user's name */
    private String name;
    /** Field user's nickname */
    private String nickname;

    /**
     * That method writes user's data in file "Users.txt"
     */
    public void writeDataInFile() {
        try(FileWriter writer = new FileWriter("Users.txt", true)) {
            String userData = makeStringFromData();
            writer.write(userData);
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Initialize field {@link Model#name}
     * @see Model
     * @param name - user's name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * initialize field {@link Model#nickname}
     * @see Model
     * @param nickname - user's nickname
     */
    public void setNickname(String nickname){
        this.nickname = nickname;
    }

    /**
     * That method makes solid string from user's data
     * @return Solid string
     */
    public String makeStringFromData() {
        return this.name + ", " + this.nickname + "\n";
    }
}