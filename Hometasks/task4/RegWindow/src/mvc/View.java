package mvc;

/**
 * That class contains notifications for user as constants
 * and utility method that prints message
 */
public class View {
    /** Special notification in case user's incorrect name */
    public static final String INCORRECT_NAME = "You entered incorrect name! It must start from capital letter," +
            "\ncontain at least 2 letters and can't be longer than 32";
    /** Special notification in case user's incorrect nickname */
    public static final String INCORRECT_NICKNAME = "You entered incorrect nickname!" +
            "\nIt can contain letters, numbers, symbols \"_\" and \"-\"," +
            "\nmust contain at least 8 letters and can't be longer than 32";
    /** Special notification in case user's successful input */
    public static final String SUCCESSFUL_INPUT = "You are registered now!";

    /**
     * Utility method. Prints message in user's terminal
     * @param message
     */
    public void printMessage(String message) {
        System.out.println(message);
    }
}