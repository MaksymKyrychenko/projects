package patternmvc;

/**
 * Created by Maksym Kyrychenko on 24.07.2021
 */
public class Model {
    private String message;

    public void ConcatenateOurStr(String msgPart) {
        if(this.message == null) {
            this.message = msgPart;
        }
        else {
            this.message += msgPart;
        }
    }

    public String getMsg() {
        return message;
    }
}