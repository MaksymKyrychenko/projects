package patternmvc;

/**
 * Created by Maksym Kyrychenko on 24.07.2021
 */
public class View {
    public static final String INCORRECT_FIRST_INPUT = "Incorrect input!" +
            "You can only write down word \"Hello\"";
    public static final String INCORRECT_SECOND_INPUT = "Incorrect input!" +
            "You can only write down word \"world\"";
    public static final String FIRST_HINT = "Write down word \"Hello\"";
    public static final String SECOND_HINT = "Write down word \"world\"";

    public void printMsg(String msg) {
        System.out.println(msg);
    }
}