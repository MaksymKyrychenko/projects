package patternmvc;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);
        Scanner sc = new Scanner(System.in);
        controller.run(sc);
    }
}