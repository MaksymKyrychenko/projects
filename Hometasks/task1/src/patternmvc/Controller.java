package patternmvc;

import java.util.Scanner;

/**
 * Created by Maksym Kyrychenko on 24.07.2021
 */
public class Controller {
    //Constructor
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void run(Scanner sc) {
        processInputMsg(sc);
        System.out.println(model.getMsg());
    }

    public void processInputMsg(Scanner sc) {
        view.printMsg(View.FIRST_HINT);
        while(true) {
            String input = sc.nextLine();
            if(!input.equalsIgnoreCase("Hello")) {
                view.printMsg(View.INCORRECT_FIRST_INPUT);
                continue;
            }
            else {
                model.ConcatenateOurStr(input);
                break;
            }
        }
        model.ConcatenateOurStr(" ");
        view.printMsg(View.SECOND_HINT);
        while(true) {
            String secondInput = sc.nextLine();
            if(!secondInput.equalsIgnoreCase("World")) {
                view.printMsg(View.INCORRECT_SECOND_INPUT);
                continue;
            }
            else {
                model.ConcatenateOurStr(secondInput);
                break;
            }
        }
    }
}